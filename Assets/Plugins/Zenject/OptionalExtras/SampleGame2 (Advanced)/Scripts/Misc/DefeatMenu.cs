﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Zenject.SpaceFighter
{

    public class DefeatMenu : MonoBehaviour
    {
        readonly Camera _camera;

        [Inject]
        readonly ControlsDisplay _controlsDisplay;

        [Inject]
        readonly PlayerGui _playerGui;

        //[Inject]
        //readonly PlayerFacade _playerFacade;

        private void OnEnable()
        {
            _playerGui.enabled = false;
            _controlsDisplay.enabled = false;
            Time.timeScale = 0;
        }

        private void OnDisable()
        {  

            Time.timeScale = 1;

        }

        public void onRetryButton()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public void onContinue()
        {
            _playerGui.enabled = true;
            _controlsDisplay.enabled = true;
            _playerGui.PlayerRefrash();
            Destroy(gameObject);
        }

        public class DefeatMenuFactory : PlaceholderFactory<PlayerGui,ControlsDisplay, DefeatMenu>
        {
        }
    }
}