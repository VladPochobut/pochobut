using UnityEngine;

namespace Zenject.SpaceFighter
{
    public class PlayerDirectionHandler : ITickable
    {
        readonly Player _player;
        readonly Camera _mainCamera;

        public PlayerDirectionHandler(
            Camera mainCamera,
            Player player)
        {
            _player = player;
            _mainCamera = mainCamera;
        }

        public void Tick()
        {
            var mouseRay = _mainCamera.ScreenPointToRay(Input.mousePosition);

            var mousePos = mouseRay.origin;
            mousePos.z = 0;

            var goalDir = mousePos - _player.Position;
            goalDir.z = 0;
            goalDir.Normalize();
            var angle = Mathf.Atan2(goalDir.y, goalDir.x) * Mathf.Rad2Deg + 180;

            _player.Rotation = Quaternion.AngleAxis(angle,Vector3.forward);
        }
    }
}
