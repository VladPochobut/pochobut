using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Zenject.SpaceFighter
{
    public class GameRestartHandler : IInitializable, IDisposable, ITickable
    {
        readonly SignalBus _signalBus;
        readonly Settings _settings;
        readonly Camera _camera;
        readonly DefeatMenu.DefeatMenuFactory _defeatMenuFactory;
        readonly ControlsDisplay _controlsDisplay;
        readonly PlayerGui _playerGui;
        //readonly PlayerFacade _playerFacade;

        bool _isDelaying;
        float _delayStartTime;

        public GameRestartHandler(
            Settings settings,
            SignalBus signalBus,
            Camera camera,
            DefeatMenu.DefeatMenuFactory defeatMenuFactory,
            ControlsDisplay controlsDisplay,
            PlayerGui playerGui
           // ,PlayerFacade playerFacade
            )
        {
            _signalBus = signalBus;
            _settings = settings;
            _camera = camera;
            _defeatMenuFactory = defeatMenuFactory;
            _controlsDisplay = controlsDisplay;
            _playerGui = playerGui;
          //  _playerFacade = playerFacade;
        }

        public void Initialize()
        {
            _signalBus.Subscribe<PlayerDiedSignal>(OnPlayerDied);
        }

        public void Dispose()
        {
            _signalBus.Unsubscribe<PlayerDiedSignal>(OnPlayerDied);
        }

        public void Tick()
        {
            if (_isDelaying)
            {
                if (Time.realtimeSinceStartup - _delayStartTime > _settings.RestartDelay)
                {
                    //TODO : RestartScene

                    _defeatMenuFactory.Create(_playerGui,_controlsDisplay);
                    _isDelaying = false;
                    _camera.enabled = false;
                }
            }
        }

        void OnPlayerDied()
        {
            // Wait a bit before restarting the scene
            _delayStartTime = Time.realtimeSinceStartup;
            _isDelaying = true;
        }


        [Serializable]
        public class Settings
        {
            public float RestartDelay;
        }
    }
}
